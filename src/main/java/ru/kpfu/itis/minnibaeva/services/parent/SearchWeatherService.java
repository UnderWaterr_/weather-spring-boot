package ru.kpfu.itis.minnibaeva.services.parent;

import ru.kpfu.itis.minnibaeva.dto.WeatherDto;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface SearchWeatherService {
    String getWeatherJSON(String url);

    Map<String, Object> parseGson(StringBuilder jsonString) throws IOException;

    List<WeatherDto> findAll();

    String getWeatherJson(Optional<String> city, Optional<String> email);

    List<WeatherDto> findAllByCity(String email);
}
