package ru.kpfu.itis.minnibaeva.services.parent;

import ru.kpfu.itis.minnibaeva.dto.RequestDto;
import ru.kpfu.itis.minnibaeva.models.Request;

import java.util.List;

public interface RequestService {
    List<RequestDto> findAllByUserId(Integer userId);
    List<RequestDto> findAllCity(String city);
}
