package ru.kpfu.itis.minnibaeva.services.parent;

import ru.kpfu.itis.minnibaeva.dto.CreateUserDto;
import ru.kpfu.itis.minnibaeva.dto.UserDto;

import java.util.List;

public interface UserService {
    List<UserDto> getAll();

    UserDto findById(Integer id);

    UserDto save(CreateUserDto user);

    UserDto signUp(CreateUserDto createUserDto, String url);

    boolean verify(String verificationCode);

    void sendVerificationMail(String mail, String name, String code, String url);
}
