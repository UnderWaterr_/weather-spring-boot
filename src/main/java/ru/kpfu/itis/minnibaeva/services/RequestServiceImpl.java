package ru.kpfu.itis.minnibaeva.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.dto.RequestDto;
import ru.kpfu.itis.minnibaeva.repositories.RequestRepository;
import ru.kpfu.itis.minnibaeva.services.parent.RequestService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RequestServiceImpl implements RequestService {


    private final RequestRepository requestRepository;

    @Autowired
    public RequestServiceImpl(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }


    @Override
    public List<RequestDto> findAllByUserId(Integer userId) {
        return requestRepository.findAllByUserId(userId).stream().map(RequestDto::fromModel).collect(Collectors.toList());
    }

    @Override
    public List<RequestDto> findAllCity(String city) {
        return requestRepository.findAllByCity(city).stream().map(RequestDto::fromModel).collect(Collectors.toList());
    }
}
