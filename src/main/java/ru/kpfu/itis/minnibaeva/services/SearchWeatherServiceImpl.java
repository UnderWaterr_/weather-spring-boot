package ru.kpfu.itis.minnibaeva.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.minnibaeva.dto.WeatherDto;
import ru.kpfu.itis.minnibaeva.models.Request;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.models.Weather;
import ru.kpfu.itis.minnibaeva.repositories.RequestRepository;
import ru.kpfu.itis.minnibaeva.repositories.UserRepository;
import ru.kpfu.itis.minnibaeva.repositories.WeatherRepository;
import ru.kpfu.itis.minnibaeva.services.parent.SearchWeatherService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SearchWeatherServiceImpl implements SearchWeatherService {

    private final RequestRepository requestRepository;
    private final UserRepository userRepository;
    private final WeatherRepository weatherRepository;

    @Autowired
    public SearchWeatherServiceImpl(RequestRepository requestRepository, UserRepository userRepository, WeatherRepository weatherRepository) {
        this.userRepository = userRepository;
        this.requestRepository = requestRepository;
        this.weatherRepository = weatherRepository;
    }


    @Override
    public String getWeatherJSON(String url) {
        try {
            URL finalUrl = new URL(url);
            HttpURLConnection connectionGet = (HttpURLConnection) finalUrl.openConnection();

            connectionGet.setRequestMethod("GET");

            StringBuilder requestResult;

            try (BufferedReader reader =
                         new BufferedReader(new InputStreamReader(connectionGet.getInputStream()))) {
                requestResult = new StringBuilder();

                String input;
                while ((input = reader.readLine()) != null) {
                    requestResult.append(input);
                }
            }
            connectionGet.disconnect();

            return requestResult.toString();

        } catch (IOException e) {
            return "404  city not found";
        }
    }

    @Override
    public Map<String, Object> parseGson(StringBuilder jsonString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Map map = mapper.readValue(jsonString.toString(), Map.class);
        Map<String, Object> mapRes = new HashMap<>();

        for (Object string : map.keySet()) {
            if (map.get(string).getClass() == LinkedHashMap.class || map.get(string).getClass() == ArrayList.class) {
                HashMap hashMap = null;

                if (map.get(string).getClass() == LinkedHashMap.class) {
                    hashMap = (HashMap) map.get(string);
                } else if (map.get(string).getClass() == ArrayList.class) {
                    ArrayList arrayList = (ArrayList) map.get(string);
                    hashMap = (HashMap) arrayList.get(0);
                }

                for (Object stringHashMap : hashMap.keySet()) {
                    mapRes.put(string + " " + stringHashMap, hashMap.get(stringHashMap));
                }

            } else {
                mapRes.put((String) string, map.get(string));
            }
        }
        return mapRes;
    }

    @Override
    public List<WeatherDto> findAll() {
        return weatherRepository.findAll().stream().map(weather -> new WeatherDto(
                weather.getId(),
                weather.getCity(),
                weather.getHumidity(),
                weather.getTime()
        )).collect(Collectors.toList());
    }

    @Override
    public String getWeatherJson(Optional<String> city, Optional<String> email) {
        StringBuilder json = new StringBuilder(this.getWeatherJSON("https://api.openweathermap.org/data/2.5/weather?q="
                + city.orElse("Kazan") + "&appid=b684cfe1558a37f5cab1c97d60108160"));

        if (email.isPresent()) {
            Optional<User> user = userRepository.getByEmail(email.get());

            if (user.isEmpty()) {
                return json + " null";
            }

            Map<String, Object> weatherParse = null;
            try {
                weatherParse = this.parseGson(json);
            } catch (IOException e) {

            }

            Weather weather = new Weather(city.orElse("Kazan"), weatherParse.get("main humidity").toString(), LocalDateTime.now());

            Request request = new Request(city.orElse("Kazan"), weather, user.get());

            requestRepository.save(request);
        }

        return json + " " + email.orElse(null);
    }

    @Override
    public List<WeatherDto> findAllByCity(String city) {
        return WeatherDto.fromModel(weatherRepository.findAllByCity(city));
    }
}
