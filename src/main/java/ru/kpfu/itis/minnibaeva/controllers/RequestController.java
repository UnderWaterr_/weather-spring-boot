package ru.kpfu.itis.minnibaeva.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.minnibaeva.dto.RequestDto;
import ru.kpfu.itis.minnibaeva.services.parent.RequestService;

import java.util.List;

@RestController
public class RequestController {

    private final RequestService requestService;

    @Autowired
    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }


    @Operation(summary = "Returns all users request by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request with user and data",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = RequestDto.class)
                            )
                    }
            )
    })
    @GetMapping("/request/by-user/{id}")
    public List<RequestDto> get(@PathVariable Integer id) {
        return requestService.findAllByUserId(id);
    }

    @Operation(summary = "Returns all requests by city")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request with user and data",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = RequestDto.class)
                            )
                    }
            )
    }) @GetMapping("/request/byCity/{city}")
    public List<RequestDto> get(@PathVariable String city) {
        return requestService.findAllCity(city);
    }
}
