package ru.kpfu.itis.minnibaeva.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kpfu.itis.minnibaeva.dto.CreateUserDto;
import ru.kpfu.itis.minnibaeva.services.parent.UserService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class SignUpController {

    private final UserService userService;

    @Autowired
    public SignUpController(UserService userService) {
        this.userService = userService;
    }


    @Operation(summary = "Returns sign up template")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Template name",
                    content = {
                            @Content(schema =
                            @Schema(implementation = String.class)
                            )
                    }
            )
    })
    @GetMapping("/signUp")
    public String getSignUp(Model model) {
        model.addAttribute("user", new CreateUserDto());
        return "sign_up";
    }

    @Operation(summary = "Returns sign up success template")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Template name",
                    content = {
                            @Content(schema =
                            @Schema(implementation = String.class)
                            )
                    }
            )
    })
    @PostMapping("/signUp")
    public String signUp(@ModelAttribute(name = "user") CreateUserDto userDto, HttpServletRequest request) {
        String url = request.getRequestURL().toString().replace(request.getServletPath(), "");
        userService.signUp(userDto, url);
        return "sign_up_success";
    }
}
