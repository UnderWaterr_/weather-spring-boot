package ru.kpfu.itis.minnibaeva.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.minnibaeva.aspect.Loggable;
import ru.kpfu.itis.minnibaeva.dto.WeatherDto;
import ru.kpfu.itis.minnibaeva.services.parent.SearchWeatherService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
public class WeatherController {

    private final SearchWeatherService service;

    @Autowired
    public WeatherController(SearchWeatherService service) {
        this.service = service;
    }


    @Operation(summary = "Returns weather by city")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "String with weather",
                    content = {
                            @Content(schema =
                            @Schema(implementation = String.class)
                            )
                    }
            )
    })
    @GetMapping("/weather")
    public String getWeatherJson(@RequestParam Optional<String> city, HttpServletRequest httpServletRequest) {
        String email = httpServletRequest.getUserPrincipal().getName();
        return service.getWeatherJson(city, Optional.of(email));
    }

    @Operation(summary = "Get all weather")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of weather entity",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = WeatherDto.class))
                    )
            )
    })
    @GetMapping("/allWeather")
    public List<WeatherDto> getAllWeatherSearch() {
        return service.findAll();
    }

    @Operation(summary = "Get all weather by city")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of weather entity",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = WeatherDto.class))
                    )
            )
    })
    @Loggable
    @GetMapping("/allWeatherByCity/{city}")
    public List<WeatherDto> getAllWeatherSearchByCity(@PathVariable String city) {
        return service.findAllByCity(city);
    }
}
