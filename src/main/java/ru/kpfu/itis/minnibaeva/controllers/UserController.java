package ru.kpfu.itis.minnibaeva.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.minnibaeva.dto.CreateUserDto;
import ru.kpfu.itis.minnibaeva.dto.UserDto;
import ru.kpfu.itis.minnibaeva.services.UserServiceImpl;
import ru.kpfu.itis.minnibaeva.services.parent.UserService;

import javax.validation.Valid;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @Operation(summary = "Returns all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of users entity",
                    content = @Content(
                            array = @ArraySchema(schema = @Schema(implementation = UserDto.class))
                    )
            )
    })
    @GetMapping("/user")
    public Iterable<UserDto> getAll() {
        return userService.getAll();
    }

    @Operation(summary = "Returns user request by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User entity",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = UserDto.class)
                            )
                    }
            )
    })
    @GetMapping("/user/{id}")
    public UserDto get(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @Operation(summary = "Create user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User entity",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = UserDto.class)
                            )
                    }
            )
    })
    @PostMapping("/user")
    public UserDto createUser(@Valid @RequestBody CreateUserDto user) {
        return userService.save(user);
    }

    @Operation(summary = "Verify account")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Result template",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = UserDto.class)
                            )
                    }
            )
    })
    @GetMapping("/verification")
    @ResponseBody
    public String verification(@Param("code") String code) {
        if (userService.verify(code)) {
            return "verification_success";
        } else {
            return "verification_failed";
        }
    }
}
