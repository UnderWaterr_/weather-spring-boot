package ru.kpfu.itis.minnibaeva.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAnnotationAspect {
    public static final Logger LOGGER = LoggerFactory.getLogger(LoggingAnnotationAspect.class);


    @Pointcut("@annotation(ru.kpfu.itis.minnibaeva.aspect.Loggable)")
    public void logGetAllWeatherSearchByCity() {

    }

    @Around("logGetAllWeatherSearchByCity()")
    public Object logAllMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object request = proceedingJoinPoint.getArgs()[0];

        Object result = proceedingJoinPoint.proceed();

        LOGGER.info("City name in request : {}", request.toString());

        return result;
    }
}
