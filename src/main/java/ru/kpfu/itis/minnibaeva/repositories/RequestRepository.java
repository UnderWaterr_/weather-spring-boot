package ru.kpfu.itis.minnibaeva.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.minnibaeva.models.Request;

import java.util.List;

public interface RequestRepository extends JpaRepository<Request, Integer> {
    List<Request> findAllByUserId(Integer userId);

    List<Request> findAllByCity(String city);
}
