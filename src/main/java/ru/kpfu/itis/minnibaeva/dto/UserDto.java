package ru.kpfu.itis.minnibaeva.dto;

import ru.kpfu.itis.minnibaeva.models.User;

import java.util.List;

public class UserDto {

    private Integer id;

    private String name;

    private String email;

    List<RequestDto> requestDtos;

    public UserDto() {
    }

    public UserDto(Integer id, String name, String email, String password) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static UserDto fromModel(User user) {
        return new UserDto(user.getId(), user.getName(), user.getEmail(), user.getPassword());
    }
}
