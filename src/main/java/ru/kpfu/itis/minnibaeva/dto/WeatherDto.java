package ru.kpfu.itis.minnibaeva.dto;

import liquibase.pro.packaged.W;
import ru.kpfu.itis.minnibaeva.models.Weather;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class WeatherDto {
    private Integer id;

    private String city;
    private String humidity;
    private LocalDateTime time;

    public WeatherDto(Integer id, String city, String humidity, LocalDateTime time) {
        this.id = id;
        this.city = city;
        this.humidity = humidity;
        this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public static WeatherDto fromModel(Weather weather) {
        return new WeatherDto(weather.getId(), weather.getCity(), weather.getHumidity(), weather.getTime());
    }

    public static List<WeatherDto> fromModel(List<Weather> weather) {
        return weather.stream().map(WeatherDto::fromModel).collect(Collectors.toList());
    }
}
