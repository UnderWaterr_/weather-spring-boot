package ru.kpfu.itis.minnibaeva.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CreateUserDto {

    @NotBlank(message = "Name shouldn't be blank!")
    @Size(min = 3, max = 15)
    private String name;

    @NotBlank(message = "Email shouldn't be blank!")
    @Size(min = 3)
    @Email
    private String email;

    @NotBlank(message = "Password shouldn't be blank!")
    @Size(min = 8, max = 64)
    private String password;

    public CreateUserDto() {
    }

    public CreateUserDto(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
