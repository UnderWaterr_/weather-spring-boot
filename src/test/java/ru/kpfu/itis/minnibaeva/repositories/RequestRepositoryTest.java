package ru.kpfu.itis.minnibaeva.repositories;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.minnibaeva.models.Request;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.models.Weather;
import ru.kpfu.itis.minnibaeva.repositories.RequestRepository;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RequestRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private RequestRepository requestRepository;

    @Test
    public void testFindAllByUserId() {
        Request request = new Request(
                "Kazan",
                new Weather("Kazan", "12", LocalDateTime.now()),
                new User("Alina", "minnibaeva@mail.ru", "testPassword")
        );
        testEntityManager.persistAndFlush(request);

        List<Request> result = requestRepository.findAllByUserId(1);
        Assert.assertEquals(result.get(0).getWeather().getHumidity(), "12");
    }

    @Test
    public void testFindAllByCity() {
        Request request = new Request(
                "Kazan",
                new Weather("Kazan", "12", LocalDateTime.now()),
                new User("Alina", "minnibaeva@mail.ru", "testPassword")
        );
        testEntityManager.persistAndFlush(request);

        List<Request> result = requestRepository.findAllByCity("Kazan");

        Assert.assertEquals(result.get(0).getWeather().getHumidity(), "12");
    }
}
