package ru.kpfu.itis.minnibaeva.repositories;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.repositories.UserRepository;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testGetByEmail() {
        User user = new User();
        user.setEmail("minnibaeva@mail.ru");
        user.setName("Alina");
        user.setPassword("testPassword");

        testEntityManager.persistAndFlush(user);

        Optional<User> result = userRepository.getByEmail("minnibaeva@mail.ru");
        Assert.assertTrue(result.isPresent());
    }

    @Test
    public void testFindByVerificationCode() {
        User user = new User();
        user.setEmail("minnibaeva@mail.ru");
        user.setName("Alina");
        user.setPassword("testPassword");
        user.setVerificationCode("1111");

        testEntityManager.persistAndFlush(user);

        User result = userRepository.findByVerificationCode("1111");
        Assert.assertEquals(result.getEmail(), "minnibaeva@mail.ru");
    }
}
