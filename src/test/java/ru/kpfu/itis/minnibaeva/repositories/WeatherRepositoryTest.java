package ru.kpfu.itis.minnibaeva.repositories;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.minnibaeva.models.Weather;
import ru.kpfu.itis.minnibaeva.repositories.WeatherRepository;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class WeatherRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private WeatherRepository weatherRepository;

    @Test
    public void testFindAllByUserId() {
        Weather weather = new Weather("Kazan", "12", LocalDateTime.now());
        testEntityManager.persistAndFlush(weather);

        List<Weather> result = weatherRepository.findAllByCity("Kazan");
        Assert.assertEquals(result.get(0).getCity(), "Kazan");
    }

}
