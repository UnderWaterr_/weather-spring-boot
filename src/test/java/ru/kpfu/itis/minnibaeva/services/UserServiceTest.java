package ru.kpfu.itis.minnibaeva.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.minnibaeva.config.MailConfig;
import ru.kpfu.itis.minnibaeva.dto.CreateUserDto;
import ru.kpfu.itis.minnibaeva.dto.UserDto;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.repositories.UserRepository;
import ru.kpfu.itis.minnibaeva.services.parent.UserService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;


@RunWith(SpringRunner.class)
public class UserServiceTest {

    @TestConfiguration
    static class UserServiceTestContextConfiguration {

        @MockBean
        private UserRepository userRepository;

        @MockBean
        private BCryptPasswordEncoder encoder;

        @MockBean
        private JavaMailSender javaMailSender;

        @MockBean
        private MailConfig mailConfig;


        @Bean
        public UserService userService() {
            return new UserServiceImpl(userRepository, encoder, javaMailSender, mailConfig);
        }
    }

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testGetAll() {
        User user = new User();
        user.setEmail("minnibaeva@mail.ru");
        user.setName("Alina");
        user.setPassword("testPassword");
        user.setVerificationCode("1111");

        given(userRepository.findAll()).willReturn(Collections.singletonList(user));
        List<UserDto> list = userService.getAll();
        Assert.assertEquals(list.get(0).getEmail(), "minnibaeva@mail.ru");
    }

    @Test
    public void testFindById() {
        User user = new User();
        user.setEmail("minnibaeva@mail.ru");
        user.setName("Alina");
        user.setPassword("testPassword");
        user.setVerificationCode("1111");

        given(userRepository.findById(1)).willReturn(Optional.of(user));
        UserDto userDto = userService.findById(1);
        Assert.assertEquals(userDto.getEmail(), "minnibaeva@mail.ru");
    }

    @Test
    public void testSignUp() {
        Assert.assertTrue(userService.getAll().isEmpty());
    }

    @Test
    public void testVerify() {
        User user = new User();
        user.setEmail("minnibaeva@mail.ru");
        user.setName("Alina");
        user.setPassword("testPassword");
        user.setVerificationCode("1111");

        given(userRepository.findByVerificationCode("1111")).willReturn(user);
        given(userRepository.findByVerificationCode("2222")).willReturn(null);
        Assert.assertTrue(userService.verify("1111"));
        Assert.assertFalse(userService.verify("2222"));
    }


    @Test
    public void testSave() {
        User user = new User();
        user.setEmail("minnibaeva@mail.ru");
        user.setName("Alina");
        user.setPassword("testPassword");
        user.setVerificationCode("1111");

        CreateUserDto createUserDto = new CreateUserDto(
                "Alina", "minnibaeva@mail.ru", "testPassword"
        );

        given(userRepository.save(any(User.class))).willReturn(user);
        UserDto userDto = userService.save(createUserDto);

        Assert.assertEquals(userDto.getEmail(), user.getEmail());
    }

}
