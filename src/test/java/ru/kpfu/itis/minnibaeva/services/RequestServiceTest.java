package ru.kpfu.itis.minnibaeva.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.minnibaeva.dto.RequestDto;
import ru.kpfu.itis.minnibaeva.models.Request;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.models.Weather;
import ru.kpfu.itis.minnibaeva.repositories.RequestRepository;
import ru.kpfu.itis.minnibaeva.services.parent.RequestService;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class RequestServiceTest {

    @TestConfiguration
    static class UserServiceTestContextConfiguration {

        @MockBean
        private RequestRepository requestRepository;

        @Bean
        public RequestService RequestServiceImpl() {
            return new RequestServiceImpl(requestRepository);
        }
    }

    @Autowired
    private RequestService requestService;

    @Autowired
    private RequestRepository requestRepository;

    @Test
    public void testFindAllByUserId() {
        Request request = new Request(
                "Kazan",
                new Weather("Kazan", "12", LocalDateTime.now()),
                new User("Alina", "minnibaeva@mail.ru", "testPassword")
        );

        given(requestRepository.findAllByUserId(1)).willReturn(Collections.singletonList(request));
        List<RequestDto> list = requestService.findAllByUserId(1);
        Assert.assertEquals(list.get(0).getWeatherDto().getCity(), "Kazan");
    }

    @Test
    public void testFindAllCity() {
        Request request = new Request(
                "Kazan",
                new Weather("Kazan", "12", LocalDateTime.now()),
                new User("Alina", "minnibaeva@mail.ru", "testPassword")
        );

        given(requestRepository.findAllByCity("Kazan")).willReturn(Collections.singletonList(request));
        List<RequestDto> list = requestService.findAllCity("Kazan");
        Assert.assertEquals(list.get(0).getWeatherDto().getCity(), "Kazan");
    }
}
