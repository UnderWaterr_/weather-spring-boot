package ru.kpfu.itis.minnibaeva.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.minnibaeva.dto.WeatherDto;
import ru.kpfu.itis.minnibaeva.models.Weather;
import ru.kpfu.itis.minnibaeva.repositories.RequestRepository;
import ru.kpfu.itis.minnibaeva.repositories.UserRepository;
import ru.kpfu.itis.minnibaeva.repositories.WeatherRepository;
import ru.kpfu.itis.minnibaeva.services.parent.SearchWeatherService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class SearchWeatherServiceTest {

    @TestConfiguration
    static class UserServiceTestContextConfiguration {

        @MockBean
        private RequestRepository requestRepository;

        @MockBean
        private UserRepository userRepository;

        @MockBean
        private WeatherRepository weatherRepository;

        @Bean
        public SearchWeatherService SearchWeatherServiceImpl() {
            return new SearchWeatherServiceImpl(requestRepository, userRepository, weatherRepository);
        }
    }

    @Autowired
    private SearchWeatherService searchWeatherService;

    @Autowired
    private WeatherRepository weatherRepository;

    @Test
    public void testGetWeatherJSON() {
        String result = searchWeatherService.getWeatherJSON("https://api.openweathermap.org/data/2.5/weather?q=Kazan&appid=b684cfe1558a37f5cab1c97d60108160");
        Assert.assertNotEquals(result, "404  city not found");
    }

    @Test
    public void testParseGson() throws IOException {
        String result = searchWeatherService.getWeatherJSON("https://api.openweathermap.org/data/2.5/weather?q=Kazan&appid=b684cfe1558a37f5cab1c97d60108160");
        Map<String, Object> parsedJson = searchWeatherService.parseGson(new StringBuilder(result));
        Assert.assertNotNull(parsedJson);
    }

    @Test
    public void testFindAll() {
        Weather weather = new Weather("Kazan", "12", LocalDateTime.now());

        given(weatherRepository.findAll()).willReturn(Collections.singletonList(weather));
        List<WeatherDto> list = searchWeatherService.findAll();
        Assert.assertEquals(list.get(0).getCity(), "Kazan");
    }

    @Test
    public void testGetWeatherJson() {

        String result = searchWeatherService.getWeatherJson(Optional.of("Kazan"), Optional.of("minnibaeva@mail.ru"));
        Assert.assertNotNull(result);

    }

    @Test
    public void testFindAllByCity() {
        Weather weather = new Weather("Kazan", "12", LocalDateTime.now());

        given(weatherRepository.findAllByCity("Kazan")).willReturn(Collections.singletonList(weather));
        List<WeatherDto> list = searchWeatherService.findAllByCity("Kazan");
        Assert.assertEquals(list.get(0).getCity(), "Kazan");
    }
}
