//package ru.kpfu.itis.minnibaeva.controller;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Order;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import ru.kpfu.itis.minnibaeva.Application;
//import ru.kpfu.itis.minnibaeva.models.User;
//import ru.kpfu.itis.minnibaeva.repository.UserRepository;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = Application.class)
//@AutoConfigureMockMvc
//@TestPropertySource(locations = "classpath:application-test.properties")
////@ActiveProfiles("test")
//public class UserControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Before
//    public void init() {
//        User user = new User();
//        user.setEmail("m@mail.ru");
//        user.setName("Ivan");
//        user.setPassword("password");
//        user.setVerificationCode("1111");
//        userRepository.save(user);
//    }
//
//    @Test
//    public void testGetAll() throws Exception {
//        mockMvc.perform(get("/user")
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[0].name").value("Ivan"));
//    }
//
//    @Test
//    public void testGetUser() throws Exception {
//        mockMvc.perform(get("/user/1")
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("name").value("Ivan"));
//
//    }
//
//    @Test
//    public void testVerification() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.
//                        get("/verification?code=1111")).
//                andExpect(status().isOk()).
//                andExpect(content().string("verification_success"));
//    }
//
//}
