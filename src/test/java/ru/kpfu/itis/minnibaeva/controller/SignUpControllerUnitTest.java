package ru.kpfu.itis.minnibaeva.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.minnibaeva.controllers.SignUpController;
import ru.kpfu.itis.minnibaeva.dto.CreateUserDto;
import ru.kpfu.itis.minnibaeva.dto.UserDto;
import ru.kpfu.itis.minnibaeva.repositories.UserRepository;
import ru.kpfu.itis.minnibaeva.services.UserServiceImpl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(SignUpController.class)
public class SignUpControllerUnitTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    @MockBean
    private UserRepository userRepository;

    @Test
    public void testGetSignUp() throws Exception {
        this.mockMvc.perform(get("/signUp"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
                .andExpect(view().name("sign_up"));

    }

    @Test
    public void testRegistration() throws Exception {
        CreateUserDto createUserDto = new CreateUserDto();
        createUserDto.setName("Alina");
        createUserDto.setPassword("testPassword");
        createUserDto.setEmail("minnibaeva@mail.ru");

        given(userService.signUp(any(CreateUserDto.class), anyString()))
                .willReturn(new UserDto(1, "Alina", "minnibaeva@mail.ru", "testPassword"));
        mockMvc.perform(post("/signUp")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .flashAttr("user", createUserDto)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
                .andExpect(view().name("sign_up_success"));
    }

}
