package ru.kpfu.itis.minnibaeva.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.minnibaeva.controllers.WeatherController;
import ru.kpfu.itis.minnibaeva.dto.WeatherDto;
import ru.kpfu.itis.minnibaeva.repositories.RequestRepository;
import ru.kpfu.itis.minnibaeva.repositories.UserRepository;
import ru.kpfu.itis.minnibaeva.repositories.WeatherRepository;
import ru.kpfu.itis.minnibaeva.services.parent.SearchWeatherService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(WeatherController.class)
public class WeatherControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SearchWeatherService weatherService;

    @MockBean
    private RequestRepository requestRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private WeatherRepository weatherRepository;

    @Before
    public void init() {
        String email = "minnibaeva@mail.ru";

        List<WeatherDto> weatherDtoList = new ArrayList<>();
        WeatherDto kazan = new WeatherDto(1, "Kazan", "12", LocalDateTime.now());
        WeatherDto moscow = new WeatherDto(2, "Moscow", "13", LocalDateTime.now());
        weatherDtoList.add(kazan);
        weatherDtoList.add(moscow);

        given(weatherService.getWeatherJson(Optional.of("Kazan"), Optional.of(email))).willReturn("Weather good");
        given(weatherService.findAll()).willReturn(weatherDtoList);
        given(weatherService.findAllByCity("Moscow")).willReturn(Collections.singletonList(moscow));
    }

//    @Test
//    public void testGetWeatherJson() throws Exception {
//        mockMvc.perform(
//                        get("/weather")
//                                .contentType(MediaType.APPLICATION_JSON)
//                )
//                .andExpect(status().isOk())
//                .andExpect(content().string("Weather good"));
//    }

    @Test
    public void testGetAllWeatherSearch() throws Exception {
        mockMvc.perform(
                get("/allWeather").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[1].city").value("Moscow"));
    }

    @Test
    public void testGetAllWeatherSearchByCity() throws Exception {
        mockMvc.perform(
                        get("/allWeatherByCity/Moscow")
                                .contentType(MediaType.APPLICATION_JSON)
                                .with(httpBasic("minnibaeva.02@gmail.com", "12345678"))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].city").value("Moscow"));
    }


}
