package ru.kpfu.itis.minnibaeva.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.minnibaeva.controllers.RequestController;
import ru.kpfu.itis.minnibaeva.dto.RequestDto;
import ru.kpfu.itis.minnibaeva.dto.WeatherDto;
import ru.kpfu.itis.minnibaeva.repositories.UserRepository;
import ru.kpfu.itis.minnibaeva.services.RequestServiceImpl;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(RequestController.class)
public class RequestControllerUnitTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RequestServiceImpl requestService;

    @MockBean
    private UserRepository userRepository;


    @Before
    public void init() {
        RequestDto requestDto = new RequestDto("Kazan",
                new WeatherDto(1, "Kazan", "12", LocalDateTime.now()));
        given(requestService.findAllByUserId(1)).willReturn(Collections.singletonList(requestDto));
        given(requestService.findAllCity("Kazan")).willReturn(Collections.singletonList(requestDto));

    }

    @Test
    public void testGetRequestByUser() throws Exception {
        mockMvc.perform(get("/request/by-user/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].city").value("Kazan"));
    }


    @Test
    public void testGetRequestByCity() throws Exception {
        mockMvc.perform(get("/request/byCity/Kazan")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].city").value("Kazan"));
    }

}
