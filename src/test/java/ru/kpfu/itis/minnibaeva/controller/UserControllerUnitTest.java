package ru.kpfu.itis.minnibaeva.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.minnibaeva.controllers.UserController;
import ru.kpfu.itis.minnibaeva.dto.UserDto;
import ru.kpfu.itis.minnibaeva.models.User;
import ru.kpfu.itis.minnibaeva.repositories.UserRepository;
import ru.kpfu.itis.minnibaeva.services.UserServiceImpl;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerUnitTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    @MockBean
    private UserRepository userRepository;

    @Before
    public void init() {
        User user = new User();
        user.setName("testName");
        given(userService.getAll()).willReturn(Arrays.asList(UserDto.fromModel(user)));
        given(userService.findById(1)).willReturn(UserDto.fromModel(user));

        given(userService.verify("111")).willReturn(true);
        given(userService.verify("222")).willReturn(false);
    }

    @Test
    public void testGetUser() throws Exception {
        mockMvc.perform(get("/user")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value("testName"));
    }

    @Test
    public void testGetUserById() throws Exception {
        mockMvc.perform(get("/user/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("name").value("testName"));
    }

    @Test
    public void testVerification() throws Exception {
        mockMvc.perform(get("/verification?code=111")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("verification_success"));
        mockMvc.perform(get("/verification?code=222")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("verification_failed"));
    }

//    @Test
//    public void testCreateUser() throws Exception {
//
//
//        given(userService.save(any(CreateUserDto.class)))
//                .willReturn(new UserDto(1, "Alina",
//                        "minnibaeva@mail.ru", "testPassword"));
//        mockMvc.perform(post("/user")
//                        .with(csrf())
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content("{\"name\": Alina, \"email\": minnibaeva@mail.ru, \"password\": testPassword}")
//                )
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
//                .andExpect(view().name("sign_up_success"));
//    }

}
